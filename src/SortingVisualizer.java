import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SortingVisualizer extends Application {

    private int windowWidth = 1280;
    private int windowHeight = 720;
    private int arraySize = 100;
    List<Integer> array;

    Group root;
    public SortingVisualizer () {
    }

    private void fillArray () {
        // Fill array with numbers
        for (int i = 1; i <= 100; i++) {
            array.add(i);
        }
        Collections.shuffle(array);
    }

    @Override
    public void start (Stage primaryStage) throws Exception {
        array = new ArrayList<>();
        root = new Group();
        Scene scene = new Scene(root, 1280, 720, Color.BLACK);
        primaryStage.setScene(scene);

        fillArray();
        drawArray();

        primaryStage.show();
    }

    private void drawArray () {
        float rectHeight = (float)windowHeight/arraySize;
        float x = windowWidth/2f, y = windowHeight - rectHeight;
        Group rectangles = new Group();
        for (int number : array) {
            float rectWidth = (float) windowWidth * number/arraySize;
            Rectangle rectangle = new Rectangle(x - rectWidth/2, y, rectWidth, rectHeight);
            rectangle.setFill(Color.WHITE); //change to using RGB
            y -= rectHeight;
            rectangles.getChildren().add(rectangle);
        }
        root.getChildren().add(rectangles);
    }

    public static void main (String[] args) {
        launch(args);
    }
}
